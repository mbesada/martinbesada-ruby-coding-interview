require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  describe "#create" do
    let(:response_body) { JSON.parse(response.body) }
    let(:valid_body) { 'This is a valid tweet' }

    context 'with valid parameters' do
      let(:user1) { create(:user)}


      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user1.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end
    end

    context'With invalid parameters' do
      let(:user1) { create(:user)}

      context 'When the body is too long' do
        let(:invalid_body) { 'a' * 181 }

        it 'returns an error response' do
          expect {
            post api_tweets_path(user_id: user1.id, body: invalid_body)
          }.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Body is too long (maximum is 180 characters)')
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user1.id, body: body)
          }.not_to change(Tweet, :count)
        end
      end

      context 'When the tweet might be a duplicate' do
        let(:existing_body) { 'This is the body'}
        let(:user1) { create(:user)}

        let(:recent_existing_tweet) do
          p "hola"
          Tweet.create!(user: user1, body: existing_body)
        end

        it 'Does not allow to create same tweet within 24 hours' do
          recent_existing_tweet
          expect {
            post api_tweets_path(user_id: user1.id, body: existing_body)
          }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Body can't post same tweet in 24 hours")
        end
      end
    end
  end
end
