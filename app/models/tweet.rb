class Tweet < ApplicationRecord
  belongs_to :user
  validates  :body, length: { maximum: 180 }
  validate :not_same_tweet_in_24_hours

  private

  def not_same_tweet_in_24_hours
    exists_same = Tweet.exists?(user: user, body: body, created_at: 24.hour.ago..Time.now)

    if exists_same
      errors.add(:body, "can't post same tweet in 24 hours")
    end
  end
end
